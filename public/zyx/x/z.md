the purpose of this page is nothing but my own reading and study and maybe commentary as my own notes for my self

verbatim from https://www.ifla.org/copyright

# Copyright

Copyright in this website belongs to IFLA.

Unless otherwise indicated, content is licensed under the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/legalcode) (CC BY 4.0), which means you are free to:
* copy
* distribute
* transmit
* adapt
* and make commercial use of the work
 
*...provided that any use is made with attribution to IFLA.*

IFLA advocates broad accessibility to knowledge for all citizens of the world, regardless of their geographical location or financial or other circumstance. IFLA undertakes many activities to achieve this goal, ranging from developing and promulgating statements about intellectual freedom, literacy, and open accessibility to scholarly communication, to advocating for improved access for consumers of information and knowledge.

Each year, IFLA, its members, and participants produce a great deal of high-value information in the form of monographs, papers, reports of professional meetings, articles and presentations as a result of research, projects, or conferences, such as the annual WLIC. Many of these works are made available through IFLA’s Web site, IFLA publications and the IFLA Library.

IFLA has a Copyright and Access Policy utilizing the Creative Commons Attribution 4.0 International (CC BY 4.0). IFLA’s web content and much of its published material is available under this licence.

