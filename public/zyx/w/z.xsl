<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
  <body>
    <table>
  <xsl:for-each select="body/p">
  <tr>
    <td><xsl:value-of select="s[1]" /></td>
    <td><xsl:value-of select="s[2]" /></td>
    <td><xsl:value-of select="s[3]" /></td>
  </tr>
  </xsl:for-each>
    </table>
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
